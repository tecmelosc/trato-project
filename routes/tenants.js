const express = require('express');
const app = express();
const Tenant=require('../models/tenant')
const co=require('co')

app.get('/:id?',function (req,res) { co(function *() {
    let id=req.params.id;
    if(id){
       let tenant = yield Tenant.findOne({_id:id})
        return res.status(200).json(tenant)
    }else{
        let tenats= yield Tenant.find()
        return res.status(200).json(tenats)
    }

}).catch(function (err) {
    console.error(err);
    res.status(500).json({
        message:"Collection not found"
    })
})
});

app.post('/',function (req,res) { co(function *() {
    let tenant = req.body;
    console.log(tenant)
    if(tenant.organization==null)
        return res.status(400).json({
            message:"The tenant name is required"
        });
    let new_tenant = new Tenant(tenant)
    let db_tenant = yield new_tenant.save();
    return res.status(200).json({
        message:"Tenant saved successfully",
        tenant:db_tenant
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:`Can't save on data base, the Tenant is duplicated`
    })
})
})


app.put('/:id',function (req,res){co(function *() {
    let tenant = req.body;
    let tenant_id = req.params.id;
    if(typeof tenant!='object')
        return res.status(400).json({
            message:"Error in tenant object"
        })

    let db_tenant = yield Tenant.findOneAndUpdate({_id:tenant_id},tenant);
    return res.status(200).json({
        message:"Tenant updated successfully",
        tenant:db_tenant
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"Can't update Tenant"
    })
})

})
//
// app.post('/privileges/:id',function (req,res) { co(function *() {
//     let privileges = req.body;
//     let tenant_id = req.params;
//
//     let tenant =  yield Tenant.findOne({_id:tenant_id});
//     let plan_id = tenant.planId;
//     let tenant_plan = yield Plan.findOne
//
//     let new_tenant = new Tenant(tenant)
//     let db_tenant = yield new_tenant.save();
//     return res.status(200).json({
//         message:"Tenant saved successfully",
//         tenant:db_tenant
//     })
// }).catch(function (err) {
//     console.error("Error",err);
//     res.status(400).json({
//         message:'Tenant not found'
//     })
// })
// })


module.exports = app;
