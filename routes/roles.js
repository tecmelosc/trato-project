const express = require('express');
const app = express();
const Roles=require('../models/role')
const co=require('co');


app.get('/:id?',function (req,res) { co(function *() {
    let id = req.params.id;
    if(id){
        try {
            let role = yield Roles.findOne({_id:id})
            return res.status(200).json(role);
        }catch (e) {
            return res.status(200).json({
                message:"Couldn't find role"
            });
        }

    }else{
        let roles= yield Roles.find()
        return res.status(200).json(roles)
    }
}).catch(function (err) {
    console.error(err);
    res.status(400).json({
        message:"Collection not found"
    })
})
});

app.post('/',function (req,res) { co(function *() {
    let role = req.body;
    if(role.name==null)
        return res.status(400).json({
            message:"The role name is required"
        })

    if(typeof role.privileges=="string")
        role.privileges=[role.privileges]
    else if(!role.privileges)
        return res.status(400).json({
            message:"The role privileges are required"
        });

        let new_role = new Roles(role)
        let db_role = yield new_role.save();
        return res.status(200).json({
            message:"Role saved successfully",
            role:db_role
        })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:`Can't save on data base, the Role is duplicated`
    })
})
})

app.put('/:id',function (req,res){co(function *() {
    let role = req.body;
    let role_id = req.params.id;
    if(typeof role!='object')
        return res.status(400).json({
            message:"Error in object role"
        })

    let db_role = yield Roles.findOneAndUpdate({_id:role_id},role);
    return res.status(200).json({
        message:"Role updated successfully",
        role:db_role
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"Can't update role"
    })
})

})

app.delete('/:id',function (req,res){co(function *() {
    let role_id = req.params.id;
    let db_role = yield Roles.findOneAndRemove({_id:role_id});
    return res.status(200).json({
        message:"Role deleted successfully",
        role:db_role
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"Role not found"
    })
})

})

module.exports = app;
