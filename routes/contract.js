const express = require('express');
const app = express();
const Contract=require('../models/contract')
const User=require('../models/user/index')
const Tenant=require('../models/tenant')
const moment = require('moment');
const co=require('co');


app.get('/',function (req,res) { co(function *() {
    let contracts= yield Contract.find().populate('user');
    return res.status(200).json(contracts)
}).catch(function (err) {
    console.error(err);
    res.status(500).json({
        message:"Collection not found"
    })
})
});

app.get('/status',function (req,res) { co(function *() {
    let status= yield Contract.distinct('status');
    return res.status(200).json(status)
}).catch(function (err) {
    console.error(err);
    res.status(500).json({
        message:"Collection not found"
    })
})
});

app.get('/filter/:tenantId/:startDate/:endDate',function (req,res) { co(function *() {
    console.log(req.params)
    const param_start_date = req.params.startDate
    const param_end_date = req.params.endDate
    let tenant=req.params.tenantId;
    let start_date;
    let end_date;
    if(param_start_date != 'null'){
        start_date = moment(req.params.startDate);
        console.log('Init date')
    }
    if(param_end_date!='null')
        end_date = moment(req.params.endDate);
    let contracts;
    if(tenant){
        contracts= yield Contract.find({tenantId:tenant})
    }else{
        contracts= yield Contract.find()
    }

    console.log(start_date, end_date)

    let array_filtered=[]
    if( param_start_date!='null'  && param_end_date!='null' ){
        console.log('both date');
        for(let contract of contracts){
            let createdAt = moment(contract.createdAt)
            if(createdAt.isBetween(start_date,end_date)){
                array_filtered.push(contract)
            }
        }
        contracts = array_filtered;
    }else if(param_start_date!='null' && param_end_date=='null'){
        console.log('After');
        for(let contract of contracts){
            let createdAt = moment(contract.createdAt)
            if(createdAt.isAfter(start_date)){
                array_filtered.push(contract)
            }
        }
        contracts = array_filtered;
    }else if(param_start_date == 'null' && param_end_date!='null'){
        console.log('Before')
        for(let contract of contracts){
            let createdAt = moment(contract.createdAt)
            if(createdAt.isBefore(end_date)){
                array_filtered.push(contract)
            }
        }
        contracts = array_filtered;
    }

    return res.status(200).json(contracts)

}).catch(function (err) {
    console.error(err);
    res.status(500).json({
        message:"Collection doesn't exist"
    })
})
});


app.post('/',function (req,res) { co(function *() {
    let contract = req.body;
    let user_id = contract.user;
    let tenant_id;

    if(!contract.name)
        res.status(400).json({
            message:"Name is required"
        })

    try {
        let user= yield User.findOne({_id:user_id});
        console.log(user)
        tenant_id=user.tenantId;
    }catch (e) {
        res.status(400).json({
            message:"Invalid User id"
        })
    }

    let contractTenantModel = Contract.byTenant(tenant_id);

    new_contract= new contractTenantModel(contract)
    let contract_db = yield new_contract.save();

    res.status(200).json({
        message:"Contract added successfully",
        contract:contract_db
    })


}).catch(function (err) {
    console.error("Error",err);
    res.status(500).json({
        message:"Contract duplicated"
    })
})
})

module.exports = app;
