const express = require('express');
const app = express();
const Plan=require('../models/plan')
const co=require('co');


app.get('/:id?',function (req,res) { co(function *() {
    let id=req.params.id;
    if(id){
        try {
            let plans= yield Plan.findOne({_id:id})
            return res.status(200).json(plans)
        }catch (err) {
            res.status(500).json({
                message:"Plan not found"
            })
        }
    }else{
        let plans= yield Plan.find()
        return res.status(200).json(plans)
    }

}).catch(function (err) {
    console.error(err);
    res.status(500).json({
        message:"Collection not found"
    })
})
});

app.post('/',function (req,res) { co(function *() {
    let plan = req.body;
    if(plan.name==null)
        return res.status(400).json({
            message:"The plan name is required"
        })

    if(typeof plan.privileges=="string")
        plan.privileges=[plan.privileges]
    else if(!plan.privileges)
        res.status(400).json({
            message:"The plan privileges are required"
        });

    let new_plan = new Plan(plan)
    let db_plan = yield new_plan.save();
    return res.status(200).json({
        message:"Plan saved successfully",
        plan:db_plan
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:`Can't save on data base, the Plan is duplicated`
    })
})
})

app.put('/:id',function (req,res){co(function *() {
    let plan = req.body;
    let plan_id = req.params.id;
    if(typeof plan!='object')
        return res.status(400).json({
            message:"Error in plan object"
        })

    let db_plan = yield Plan.findOneAndUpdate({_id:plan_id},plan);
    return res.status(200).json({
        message:"Plan updated successfully",
        plan:db_plan
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"Can't update Plan"
    })
})

})

app.delete('/:id',function (req,res){co(function *() {
    let plan_id = req.params.id;
    let db_plan = yield Plan.findOneAndRemove({_id:plan_id});
    return res.status(200).json({
        message:"Plan deleted successfully",
        plan:db_plan
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"Plan not found"
    })
})

})

module.exports = app;
