const express = require('express');
const app=express();
//Importing Routes
const users = require('./users');
const tenants = require('./tenants');
const contracts = require('./contract');
const roles = require('./roles');
const plans = require('./plans');






//Setting the routes to the app
app.use('/user',users);
app.use('/tenant',tenants);
app.use('/contract',contracts);
app.use('/role',roles);
app.use('/plan',plans);

module.exports = app;



