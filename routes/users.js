const express = require('express');
const app = express();
const User=require('../models/user/index')
const Tenant=require('../models/tenant')
const Contract = require('../models/contract')

const co=require('co')

app.get('/:id?',function (req,res) { co(function *() {
    let id=req.params.id;
    if(id){
        try {
            let user= yield User.findOne({_id:id}).populate('contracts','name');
            return res.status(200).json(user)
        }catch (e) {
            return res.status(400).json({
                message:'User not found'
            })
        }
    }else{
        let users= yield User.find().populate('contracts','name')
        return res.status(200).json(users)
    }

}).catch(function (err) {
    console.error(err);
    res.status(400).json({
        message:"Collection not found"
    })
})
});



app.post('/',function (req,res) { co(function *() {
    let user = req.body;
    let organization= user.organization;

    if(!user.email)
        res.status(400).json({
            message:"Email is required"
        })
    try {
        let tenant= yield Tenant.findOne({organization:organization})
    }catch (e) {
        res.status(400).json({
            message:"Invalid Tenant name"
        })
    }


    new_user= new User(user)
    let user_db = yield new_user.save();
    console.log(new_user);

    res.status(200).json({
        message:"User added successfully",
        user:user_db
    })
}).catch(function (err) {
    console.error(err);
    res.status(500).json({
        message:"User duplicated"
    })
})
})

app.put('/status/:iduser/:iduserto?',function (req,res){co(function *() {
    let user = req.body;
    let id_user=req.params.iduser;
    let id_userto=req.params.iduserto;
    let db_user = yield User.findOneAndUpdate({_id:id_user},user);
    let db_contracts;
    if(!user.twoFactor.enabled){
        console.log('Migrating contracts')
        db_contracts = yield Contract.updateMany({user:id_user},{user: id_userto})
    }
    return res.status(200).json({
        message:"User status updated"
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"Can't update User"
    })
})

})


app.put('/:id',function (req,res){co(function *() {
    let user = req.body;
    let user_id = req.params.id;
    if(typeof user!='object')
        return res.status(400).json({
            message:"Error in User object"
        })

    let db_user = yield User.findOneAndUpdate({_id:user_id},user);
    return res.status(200).json({
        message:"User updated successfully",
        user:db_user
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"Can't update User"
    })
})

})


app.put('/activate/:id',function (req,res){co(function *() {
    let user_id = req.params.id;
    let db_user = yield User.findOneAndUpdate({_id:user_id},{userIsClient:true});
    return res.status(200).json({
        message:"User activated successfully",
        user:db_user
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"User couldn't be activated"
    })
})
})


app.put('/deactivate/:id',function (req,res){co(function *() {
    let user_id = req.params.id;
    let db_user = yield User.findOneAndUpdate({_id:user_id},{userIsClient:false});
    return res.status(200).json({
        message:"User deactivate successfully",
        user:db_user
    })
}).catch(function (err) {
    console.error("Error",err);
    res.status(400).json({
        message:"User couldn't be deactivate"
    })
})

})
//  app.post('/', (req,res)=>{
//     let user= req.body;
//     if(typeof user.name == 'undefined'){
//         return res.status(500).json({
//             message:"The name field is required"
//         })
//     }
//     if(typeof user.lastName == 'undefined'){
//         return res.status(500).json({
//             message:"The lastname field is required"
//         })
//     }
//     if(typeof user.email == 'undefined'){
//         return res.status(500).json({
//             message:"The email field is required"
//         })
//     }
//     let user_doc= new User({
//         name:user.name,
//         email:user.email,
//         lastName:user.lastName,
//         telephone:user.telephone,
//         address:user.address
//     })
//     let user_db = user_doc.save();
//     console.log(user_db)
//      if (user_db){
//          return res.status(200).json({
//              message:"User has been added to the data base successfully",
//              user: user_db
//          })
//      }else{
//          console.error("User coudln't add to data base",user_db);
//          return res.status(200).json({
//              message:"Couldn't add the user to data base",
//              user: doc
//          })
//      }
// });
//
// app.get('/:id*?', (req,res)=>{
//     if(typeof req.params.id == "undefined"){
//         let collection  =  User.find({});
//         if(collection){
//             return res.status(200).json(collection)
//         }else{
//             return res.status(500).json({
//                 message:"Collection not available"
//             })
//         }
//     }else{
//         try {
//             let id = req.params.id;
//             let user_db= User.findOne({_id:id});
//             if(user_db){
//                 return res.status(200).json(user_db)
//             }else{
//                 return res.status(404).json({
//                     message:"User couldn't be found"
//                 })
//             }
//         }catch (e) {
//             return res.status(500).json({
//                 message:"User not found"
//             })
//         }
//     }
//     });
//
// app.put('/:id', (req,res)=>{
//     let id=req.params.id;
//     let modified_user = req.body;
//     try {
//         let user_db =  User.findOneAndUpdate({_id:id},modified_user,{ new: true, runValidators: true })
//         if(user_db){
//             return res.status(200).json(user_db)
//         }else{
//             return res.status(500).json({
//                 message:"User couldn't be modified"
//             })
//         }
//     }catch (e) {
//         return res.status(500).json({
//             message:"User not found"
//         })
//     }
//
// })
//
// app.delete('/:id', (req,res)=>{
//     let id=req.params.id;
//     try {
//         let deleted_user =  User.findOneAndRemove({_id:id})
//         if(deleted_user){
//             return res.status(200).json({
//                 message:"User has been deleted successfully",
//                 deteled_user:deleted_user})
//         }else{
//             return res.status(500).json({
//                 message:"User couldn't be deleted"
//             })
//         }
//     }catch (e) {
//         return res.status(500).json({
//             message:"User not found"
//         })
//     }
// })

module.exports = app;
