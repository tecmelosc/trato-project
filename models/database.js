const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const databaseSchema = new Schema({
	keyColumn: String,
	dataset: Object
}, { timestamps: true, usePushEach: true });

module.exports = mongoose.model('Database', databaseSchema);
