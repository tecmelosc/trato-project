const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const commentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    email: String,
    message: {
        type: String,
        required: true,
        trim: true
    },
    name: String
}, { timestamps: true });

module.exports = commentSchema;
