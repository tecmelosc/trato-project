const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const fadFiles = new Schema({
    fileName: String,
    url: String,
    extension: String,
    description: Schema.Types.Mixed
}, { timestamps: false });

module.exports = fadFiles;
