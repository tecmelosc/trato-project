const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const signatureSchema = new Schema({
    page: {
        type: Number,
        default: 1
    },
    left: {
        type: Number,
        default: 0
    },
    top: {
        type: Number,
        default: 0
    },
    width: {
        type: Number,
        default: 300
    },
    height: {
        type: Number,
        default: 300
    }
}, { timestamps: true, minimize: false });

module.exports = signatureSchema;
