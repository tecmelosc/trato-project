const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const changelogSchema = new Schema({
    user: String,
    url: String,
    type: String,
    versionId: String
}, { timestamps: true });

module.exports = changelogSchema;
