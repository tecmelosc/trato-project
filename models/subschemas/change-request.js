const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const changeRequestSchema = new Schema({
    request: String,
    attendedAt: {
        type: Date
    },
    attended: {
        type: Boolean,
        default: false
    },
    requested: {
        type: Boolean,
        default: false
    },
    requestedAt: {
        type: Date
    },
    applicant: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    tasks: [{
        description: { type: String },
        attended: { type: Boolean, default: false },
        comments : { type: String }
    }]
}, { timestamps: true });

module.exports = changeRequestSchema;
