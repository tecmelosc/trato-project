const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const versionSchema = require('./versions');

const wordOnlineSchema = new Schema({
    id: String,
    key: String,
    currentVersion: String,
    documentType: String,
    LockValue: String,
    LockExpires: Date,
    OwnerId: String,
    BaseFileName: String,
    Container: String,
    Size: Number,
    Version: String,
    UserInfo: String,
    versions: [ versionSchema ]
}, { timestamps: false });

module.exports = wordOnlineSchema;
