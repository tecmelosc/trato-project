const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const versionSchema = require('./versions');

const workdocsSchema = new Schema({
    id: String,
    currentVersion: String,
    documentType: String,
    versions: [ versionSchema ]
}, { timestamps: false });

module.exports = workdocsSchema;
