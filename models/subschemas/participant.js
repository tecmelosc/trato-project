const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const attachmentSchema = require('./attachment');
const variablesSchema = require('./variable');
const signatureSchema = require('./signature');

const participantSchema = new Schema({
    label: {
        type: String,
        required: true,
        trim: true
    },
    name: {
        type: String,
        trim: true
    },
    last_name: {
        type: String,
        trim: true
    },
    linkExpirationDate: {
        type: Date
    },
    representative: {
        type: String,
        trim: true
    },
    variables: [ variablesSchema ],
    identification: {
        type: Object
    },
    address: {
        type: Object
    },
    company: {
        type: Object
    },
    statusDate: {
        type: Date
    },
    status: {
        type: String
    },
    requestBasicInformation: {
        type: Boolean,
        default: false
    },
    disableBasicInformation: {
        type: Boolean,
        default: true
    },
    obligation: {
        required: true,
        type: String,
        trim: true
    },
    signOrder:{
        type: Number
    },
    email: {
        type: String,
        trim: true
        /*validate(value) {
            return value.indexOf('@') > -1 || !value;
        }*/
    },
    delegatedFrom: String,
    originalSigner: String,
    phone: {
        type: String,
        default: ''
    },
    filledAt: {
        type: Date
    },
    signedAt: {
        type: Date
    },
    signature: String,
    privacyAgreement: {
        type: Boolean,
        default: false
    },
    requestAgreement: {
        type: Boolean,
        default: false
    },
    videoValidated: {
        type: Boolean,
        default: false
    },
    ip: String,
    audio: String,
    video: String,
    photo: String,
    notify: {
        type: Boolean,
        default: false
    },
    attachments: [ attachmentSchema ],
    message: String,
    token: String,
    jwt: String,
    left: {
        type: Number,
        default: 0
    },
    top: {
        type: Number,
        default: 0
    },
    width: {
        type: Number,
        default: 300
    },
    height: {
        type: Number,
        default: 300
    },
    fadSignature: Boolean,
    signatures: [ signatureSchema ],
    externalFieldName: String
});

module.exports = participantSchema;
