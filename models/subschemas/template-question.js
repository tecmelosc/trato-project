const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const answerSchema = require('./template-answer');

const questionSchema = new Schema({
    content: String,
    type: String,
    answers: [ answerSchema ]
}, { timestamps: true, minimize: false });


module.exports = questionSchema;
