const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const lawSchema = new Schema({
    name: String,
    article: [ String ]
}, { timestamps: true, minimize: false });

module.exports = lawSchema;
