

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const tinyCommentsSchema = new Schema({
    id: Schema.Types.ObjectId,
    permissions: Schema.Types.Mixed,
    quote: String,
    ranges: Array,
    tags: Array,
    text: String,
    user: String
}, { timestamps: true });

module.exports = tinyCommentsSchema;
