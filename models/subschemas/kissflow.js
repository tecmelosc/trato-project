const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const kissFlowSchema = new Schema({
    api_key: String,
    accountId: String,
    mapping: [
        {
            processName: String,
            mapping: Array
        }
    ]
}, { timestamps: false });


module.exports = kissFlowSchema;
