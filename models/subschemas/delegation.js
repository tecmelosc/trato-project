const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const delegation = new Schema({
    user_who: {
			type: Schema.Types.ObjectId,
			required: false,
			ref: 'User'
		},
		user_to: {
				type: Schema.Types.ObjectId,
				required: false,
				ref: 'User'
		},
		delegatedAt: Date,
		current: {
			type: Boolean,
			default: false
		}
}, { timestamps: false });

module.exports = delegation;
