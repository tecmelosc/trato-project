const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const historySchema = new Schema({
    type: String,
    change: Object,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, { timestamps: true });

module.exports = historySchema;
