const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const attachmentSchema = new Schema({
    name: String,
    filename: String,
    comment: String,
    data: String,
    locked: {
        type: Boolean,
        default: false
    },
    required: {
        type: Boolean,
        default: true
    },
    blockchained: {
        type: Boolean,
        default: false
    },
    blockchain: {
        hash: String,
        transactionId: String,
        hashedAt: {
            type: Date
        }
    },
    subordinated: {
        type: Boolean,
        default: false
    },
    expire: {
        type: Date
    },
    url: String,
    validated: {
      type: Boolean,
      default: false
    }
}, { timestamps: true });

module.exports = attachmentSchema;
