const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const subordinationSchema = new Schema(
    {
        template: {
            type: Schema.Types.ObjectId,
            ref: 'Template',
            index: true,
            required: false
        },
        contract: {
            type: Schema.Types.ObjectId,
            ref: 'Contract',
            index: true,
            required: false
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: false
        },
        userGroup: {
            type: Schema.Types.ObjectId,
            ref: 'UserGroup',
            required: false
        },
        properties: Object
    },
    { timestamps: false }
);

module.exports = subordinationSchema;
