const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const versionSchema = new Schema(
    {
        user: String,
        userGroup: String,
        reviewer: String,
        content: String,
        number: Number
    },
    { timestamps: true }
);

module.exports = versionSchema;
