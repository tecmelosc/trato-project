const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const referenceSchema = new Schema({
  document: {
      type: Schema.Types.ObjectId,
      ref: 'Contract'
  },
  validated: {
    type: Boolean,
    default: false
  }
}, { timestamps: true });

module.exports = referenceSchema;
