const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const versionSchema = new Schema({
    id: String,
    modified: Date,
    url: String,
    type: String,
    versionId: String,
    user: String,
    size: String,
    comparedRight: String,
    comparedLeft: String
}, { timestamps: false });

module.exports = versionSchema;
