const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const variableSchema = new Schema({
    name: String,
    apiName: String,
    type: String,
    value: String,
    limit: Number,
    options: [ String ],
    dataset: String

}, { timestamps: false });

module.exports = variableSchema;
