const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const workflowstepsSchema = new Schema({
    order: Number,
    type: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, { timestamps: false });

module.exports = workflowstepsSchema;
