const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const tenantConfigProperties = new Schema({
    name: String,
    value: String,
    active: {
        type: Boolean,
        default: true
    }
}, { timestamps: false });


module.exports = tenantConfigProperties;
