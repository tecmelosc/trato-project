const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const paymentSchema = new Schema({
    number: Number,
    amount: Number,
		paid: {
			type: Boolean,
			default: false
		},
		paid_date: {
			type: Date
		},
		due_date: {
			type: Date
		}
});


module.exports = paymentSchema;
