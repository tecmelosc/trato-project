const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const changeSchema = new Schema({
    date: Date,
    direction: String,
    type: String,
    value: String,
    status: String
});

module.exports = changeSchema;
