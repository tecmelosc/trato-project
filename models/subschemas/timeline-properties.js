const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const timelineProperties = new Schema({
    participants: Array,
    attachment: String,
    comment: String,
    url: String,
    document: String,
    filename: String,
    date: String
}, { timestamps: false });

module.exports = timelineProperties;
