const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const memberSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    userGroup: {
        type: Schema.Types.ObjectId,
        ref: 'UserGroup'
    },
    status: String,
    dateStartRevision: { type: Date },
    dateEndRevision: { type: Date },
    dueDays: { type: Number }
}, { timestamps: true });

module.exports = memberSchema;
