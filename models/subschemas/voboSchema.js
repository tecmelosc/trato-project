const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');
const Schema = mongoose.Schema;
const voboSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    userGroup: {
        type: Schema.Types.ObjectId,
        ref: 'UserGroup'
    },
    reason: String,
    status: String,
    dateStartVoBo: { type: Date },
    dateEndVoBo: { type: Date }
    // dueDays: { type: Number }
}, { timestamps: true });

module.exports = voboSchema;
