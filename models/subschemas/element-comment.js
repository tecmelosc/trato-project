const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const commentSchema = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: false
        },
        userGroup: {
            type: Schema.Types.ObjectId,
            ref: 'UserGroup',
            required: false
        },
        email: String,
        message: {
            type: String,
            required: true,
            trim: true
        },
        element: {
            item: String,
            content: String
        },
        answers: [
            {
                user: {
                    type: Schema.Types.ObjectId,
                    ref: 'User',
                    required: false
                },
                userGroup: {
                    type: Schema.Types.ObjectId,
                    ref: 'UserGroup',
                    required: false
                },
                email: String,
                message: {
                    type: String,
                    required: true,
                    trim: true
                },
                createdAt: {
                    type: Date,
                    default: Date.now
                }
            }
        ]
    },
    { timestamps: true }
);

module.exports = commentSchema;
