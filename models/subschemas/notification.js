const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const recipientSchema = require('./recipient');

const notificationSchema = new Schema({
    name: {
        type: String,
        trim: true
    },
    message: {
        type: String,
        trim: true
    },
    startDate: {
        type: Date
    },
    sendDay: {
        type: Number
    },
    endDate: {
        type: Date
    },
    sendDate: {
        type: Date
    },
    sendTime: String,
    active: {
        type: Boolean,
        default: false
    },
    type: String,
    frequency: String,
    recipients: [ { type: String } ]
});


module.exports = notificationSchema;
