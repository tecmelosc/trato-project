const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const elementReviewerSchema = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            required: false,
            ref: 'User'
        },
        userGroup: {
            type: Schema.Types.ObjectId,
            required: false,
            ref: 'UserGroup'
        },
        edit: Boolean,
        view: Boolean,
        review: Boolean,
        reviewed: {
            type: Boolean,
            default: false
        }
    // reviewed: {
    //   type: Boolean,
    //   default: false
    // }
    },
    { timestamps: true }
);

module.exports = elementReviewerSchema;
