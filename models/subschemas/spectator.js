const mongoose = require('mongoose');
const attachmentSchema = require('./attachment');

const Schema = mongoose.Schema;

const spectatorSchema = new Schema({
    name: {
        type: String,
        trim: true
    },
    last_name: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        /*validate(value) {
            return value.indexOf('@') > -1 || !value;
        }*/
    },
    phone: {
        type: String,
        default: ''
    },
    linkExpirationDate: {
        type: Date
    },
    attachments: [ attachmentSchema ],
    jwt: String
});

module.exports = spectatorSchema;
