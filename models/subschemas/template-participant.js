const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const attachmentSchema = require('./attachment');

const templateParticipant = new Schema({
    label: {
        type: String,
        required: true,
        trim: true
    },
    attachments: [ attachmentSchema ],
    obligation: {
        type: String,
        required: true,
        trim: true,
        default: 'individual'
    },
    variables: Array
});

module.exports = templateParticipant;
