const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const recipientSchema = new Schema({
    name: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true
    }
});

module.exports = recipientSchema;
