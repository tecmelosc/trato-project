const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const contractProperties = new Schema({
    ammount: Number,
    payment_number: Number
}, { timestamps: true });

module.exports = contractProperties;
