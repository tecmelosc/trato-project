const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const answerActionSchema = require('./template-answer-action');

const answerSchema = new Schema({
    content: String,
    actions: [ answerActionSchema ]
}, {
    timestamps: true,
    minimize: false
});

module.exports = answerSchema;
