const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const automationSchema = new Schema({
    status: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    action: String,
    step: Number
}, { timestamps: true });

module.exports = automationSchema;
