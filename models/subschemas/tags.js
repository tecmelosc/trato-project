const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const tagSchema = new Schema({
    name: String,
    value: String,
    type: String,
    subtype: String,
    auto: {
        type: Boolean,
        default: false
    },
    deleted: {
        type: Boolean,
        default: false
    },
    notification: {
        type: Boolean,
        default: false
    },
    variable: Schema.Types.ObjectId,
    message: String,
    daysBefore: Array
}, { timestamps: true, minimize: false });

// elementSchema.plugin(deepPopulate);

module.exports = tagSchema;
