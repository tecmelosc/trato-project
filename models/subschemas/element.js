const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const elementComment = require('./element-comment');
const elementVersion = require('./element-version');
const elementReviewer = require('./element-reviewer');

const elementSchema = new Schema({
    locked: {
        type: Boolean,
        default: true
    },
    type: {
        type: String,
        required: true,
        index: true,
        validate(value) {
            return [ 'table', 'document_logo', 'title', 'attachment', 'clause', 'declaration', 'text', 'header', 'footer', 'expiry', 'signature', 'statement', 'break', 'document_header', 'document_footer' ].indexOf(value) > -1;
        }
    },
    block: Number,
    number: Number,
    index: Number,
    fileName: String,
    fileSize: String,
    favorite: Boolean,
    currentVersion: {
        type: Number,
        default: 0
    },
    height: {
        type: Number
    },
    content: String,
    comments: [ elementComment ],
    signComments: [ elementComment ],
    reviewComments: [ elementComment ],
    versions: [ elementVersion ],
    restart: {
        type: Boolean,
        default: true
    },
    reviewers:[ elementReviewer ],
    checked: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, minimize: false });

// elementSchema.plugin(deepPopulate);


module.exports = elementSchema;
