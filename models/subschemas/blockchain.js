const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');
const Schema = mongoose.Schema;

const blockchainSchema = new Schema({
    active: {
        type: Boolean,
        default: false
    },
    type: String,
    hash: String,
    txid: String,
    date: Date,
    properties: Object
}, { timestamps: true });

module.exports = blockchainSchema;
