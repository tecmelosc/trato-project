const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const answerActionSchema = new Schema({
    content: String,
    action: String,
    element: String,
    item: String
}, {
    timestamps: true,
    minimize: false
});

module.exports = answerActionSchema;
