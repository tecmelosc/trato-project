const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is required']
    },
    lastName: {
        type: String,
        required: [true, 'The lastname is required']
    },
    email: {
        type: String,
        required: [true,'The email is required']
    },
    telephone: {
        type: Number,
        default: false
    },
    address: {
        type: String,
        default: false
    }
});
module.exports = mongoose.model('user', userSchema);