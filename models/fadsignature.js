const mongoose = require('mongoose');
//const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;
const fadFiles = require('./subschemas/fadfiles');
const fadSignatureSchema = new Schema({
    requisitionId: {
        type: Schema.Types.String,
        index: true
    },
    url: String,
    key: String,
    vector: String,
    status: String,
    completed: {
        type: Schema.Types.Boolean,
        default: false
    },
    ticket: String,
    contract: {
        type: Schema.Types.ObjectId,
        ref: 'Contract'
    },
    password: String,
    files : [ fadFiles ]
}, { timestamps: true });

//fadSignatureSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('FadSignature', fadSignatureSchema);
