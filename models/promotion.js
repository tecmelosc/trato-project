

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const promotionSchema = new Schema({
    name: String,
    options: {}
}, { timestamps: true });

promotionSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Promotion', promotionSchema);
