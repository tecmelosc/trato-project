

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const creditSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    type: {
        type: String,
        default: '-',
        validate(value) {
            if (value === '+' || value === '-') return true;
            return false;
        }
    },
    amount: {
        type: Number,
        required: true,
        default: 0
    },
    usage: {
        type: 'String'
    },
    usageId: {
        type: Schema.Types.ObjectId
    }
}, { timestamps: true });

creditSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Credit', creditSchema);
