

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const folderSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    childOf: {
        type: String,
        default: undefined
    },
    type: {
        type: String,
        enum: [ 'contracts', 'templates' ],
        default: 'contracts'
    }
}, { timestamps: true });

folderSchema.pre('save', function(next) {
    const self = this;
    console.log('user in folders ', self.user);
    console.log('name in folders ', self.name);
    if (self.childOf || self.name === 'root') return next();
    console.log('checkando queexista el root ',
        mongoose.Types.ObjectId.isValid(self.user.valueOf().toString()), {
            name:'root',
            user:  self.user.valueOf().toString(),
            type: self.type || 'contracts'
        });
    Folder
        .findOne({
            name:'root',
            user:  mongoose.Types.ObjectId(self.user.toString()),
            type: self.type || 'contracts'
        })
        .lean()
        .exec()
        .then((folder) => {
            console.log('folders ', folder);
            if(!folder) console.log('data to create root', self,
                self.user, {
                    name:'root',
                    user: self.user,
                    type: self.type || 'contracts'
                });
            if (!folder) return Folder/*byTenant(tenantId)*/.create({
                name:'root',
                user: self.user,
                type: self.type || 'contracts'
            });
            return folder;
        })
        .then((folder) => {
            console.log('folder root ', folder);
            self.childOf = folder._id;
            next();
        })
        .catch(next);
});

folderSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

folderSchema.index({ user:1, name:1, childOf:1, type: 1 }, { unique:true });
const Folder = module.exports = mongoose.model('Folder', folderSchema);
