

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const refererSchema = new Schema({
    refBy: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    verificationHash: {
        type: String,
        required: true,
        trim: true
    },
    status: {
        type: String,
        validate(val) {
            return [ 'invited', 'accepted' ].indexOf(val) > -1;
        }
    }
}, { timestamps: true });

refererSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Referer', refererSchema);
