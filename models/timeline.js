

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;
const timelinePropertiesSchema = require('./subschemas/timeline-properties');

const timelineSchema = new Schema(
    {
        type: String,
        contract: {
            type: Schema.Types.ObjectId,
            ref: 'Contract'
        },
        message: String,
        isPostSend: {
            type: Boolean,
            default: false
        },
        properties: timelinePropertiesSchema,
        eventDate: {
            type: Date,
            default: Date.now
        },
        user_who: {
          type: Schema.Types.ObjectId,
          ref: 'User'
        },
        user_to: {
          type: Schema.Types.ObjectId,
          ref: 'User'
        },
        who: String,
        to: String,
        blockchainHash: String,
        blockchainTx: String
    },
    { timestamps: false }
);

timelineSchema.pre('save', function(next) {
    //console.log('timeline', this);
    next();
});

timelineSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Timeline', timelineSchema);
