

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const activitySchema = new Schema({
    type: String, // SENT
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    to: String,
    contract: {
        type: Schema.Types.ObjectId,
        ref: 'Contract'
    },
    template: {
        type: Schema.Types.ObjectId,
        ref: 'Template'
    }
}, { timestamps: true });

activitySchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Activity', activitySchema);
