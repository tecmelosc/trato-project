

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const ParticipantSchema = new Schema({
    email: {
        type: String,
        index: true
    },
    firstTime: {
        type: Boolean
    },
    name: {
        type: String
    },
    last_name: {
        type: String
    },
    phone: {
        type: String
    },
    privateAgreement: {
        type: Boolean
    }
}, { timestamps: true });

ParticipantSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Participant', ParticipantSchema);
