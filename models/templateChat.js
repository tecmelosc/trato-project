

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const chatObject = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    text: {
        type: String,
        required: true
    },
    self: {
        type: Boolean,
        required: false
    }
}, {
    timestamps: true,
    minimize: false
});

const templateChat = new Schema({
    template: {
        type: Schema.Types.ObjectId,
        ref: 'Template'
    },
    client: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    chat: [ chatObject ]
}, {
    timestamps: true,
    minimize: false
});

templateChat.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('TemplateChat', templateChat);
