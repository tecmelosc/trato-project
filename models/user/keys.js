

const mongoose = require('mongoose');

module.exports = {
    name:{
        type:String
    },
    isAuthor: {
        type: Boolean,
        default: false
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    charges: [],
    userIsClient: { //HARCODED STUFF 😠
      type: Boolean,
      default: false
    },
    accountAdmin: {
        type: Boolean,
        default: false
    },
    userRoles:  Array,
    roles: [ { type: String } ],
    accountRoot: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    language: {
        type: String,
        default: 'ES'
    },
    // plan: {
    //     validUntil: {
    //         type: Date
    //     },
    //     type: {
    //         type: String
    //     },
    //     name: String,
    //     details: Object,
    //     contracts: Number,
    //     chargeDay: Number,
    //     users: Number
    // },
    twoFactor: {
        enabled: {
            type: Boolean,
            default: false
        },
        secret: Object
    },
    stripeCustomerId: String,
    stripeSubscriptionId: String,
    salt: {
        type: String,
        trim: true,
        required: true
    },
    verified: {
        type: Boolean,
        default: false
    },
    verificationHash: {
        type: String,
        trim: true
    },
    invoice: {
        name: String,
        rfc: String,
        address: String
    },
    givenName: {
        type: String,
        default: ''
    },
    familyName: {
        type: String,
        default: ''
    },
    phone: {
        type: String,
        default: ''
    },
    organization: {
        type: String
    },
    photo: {
        type: String
    },
    lastRead: Date,
    office: {
        representative: String,
        name: {
            type: String,
            trim: true
        },
        website: String,
        logo: String,
        description: String,
        color: {
            type: String,
            default: '#344e5b'
        }
    },
    // permission: {
    //     type: String,
    //     default: ''
    // },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    // usedCupons: [],
    admin: Boolean,
    contracts: [],
    demo: Boolean,
    // potentialReferrer: [],
    authorName: String,
    hasCancelled: {
        type: Boolean,
        default: false
    },
    lawyerOnly: {
        type: Boolean,
        default: false
    },
    signature: {
        type: String,
        default: null
    },
    // branding: {
    //     appStoreLink: String,
    //     playStoreLink: String,
    //     appStoreImage: String,
    //     playStoreImage: String
    // },
    groups: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'UserGroup'
        }
    ]
};
