const moment = require('moment');

module.exports.createOrUpdateContact = (self, client, resolve) => client.contacts.createOrUpdate(
    self.email,
    {
        properties: [ {
            property: 'email',
            value: self.email
        }, {
            property: 'firstname',
            value: self.givenName
        }, {
            property: 'lastname',
            value: self.familyName
        }, {
            property: 'phone',
            value: self.phone
        } ]
    }, (err) => {
        if (err) console.error(err);
        return resolve();
    }
);

module.exports.addSubscription = (self, client, resolve) => client.contacts.createOrUpdate(
    self.email,
    {
        properties: [ {
            property: 'became_subscriptor',
            value: moment().utc().startOf('day').unix() * 1000
        } ]
    }, (err) => {
        if (err) console.error(err);
        return resolve();
    }
);

module.exports.addCredit = (self, client, resolve) => client.contacts.createOrUpdate(
    self.email,
    {
        properties: [ {
            property: 'added_credit',
            value: moment().utc().startOf('day').unix() * 1000
        } ]
    }, (err) => {
        if (err) console.error(err);
        return resolve();
    }
);

module.exports.createContract = (self, client, resolve) => client.contacts.createOrUpdate(
    self.email,
    {
        properties: [ {
            property: 'created_contract',
            value: moment().utc().startOf('day').unix() * 1000
        } ]
    }, (err) => {
        if (err) console.error(err);
        return resolve();
    }
);

module.exports.contractSign = (self, client, resolve) => client.contacts.createOrUpdate(
    self.email,
    {
        properties: [ {
            property: 'contract_sign',
            value: moment().utc().startOf('day').unix() * 1000
        } ]
    }, (err) => {
        if (err) console.error(err);
        return resolve();
    }
);

module.exports.paymentFailed = (self, client, resolve) => client.contacts.createOrUpdate(
    self.email,
    {
        properties: [ {
            property: 'payment_failed',
            value: moment().utc().startOf('day').unix() * 1000
        } ]
    }, (err) => {
        if (err) console.error(err);
        return resolve();
    }
);
