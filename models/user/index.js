

const _ = require('underscore');
const moment = require('moment');
const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;
const keys = require('./keys');
const co = require('co');
const Credit = require('../credit');
const Contract = require('../contract');
//const User = mongoose.model('User');
const Referer = require('../referer');
const InstantNotification = require('../instant-notification');
const Task = require('../tasks');
const ContractLink = require('../contract-link');
// const emails = require('../../services/emails');
// const counter = require('../../services/counters');
// const intercom = require('../../integrations/intercom');
// const agile = require('../../integrations/agile');

const VIRTUAL = {
    // DRAFT: 'DRAFT',
    // NEGOTIATION: 'NEGOTIATION',
    SIGNED: 'SIGNED',
    TO_SIGN: 'TO_SIGN',
    // TO_FILL: 'TO_FILL',
    // VALIDATION: 'VALIDATION',
    PENDING: 'PENDING'
};
const STATES = {
    AUTHORIZE: VIRTUAL.PENDING,
    NEGOTIATION: VIRTUAL.PENDING,
    PENDING_VALIDATION: VIRTUAL.PENDING,
    PENDING_NEGOTIATION: VIRTUAL.PENDING,
    RECEIVED_FILLED: VIRTUAL.TO_SIGN,
    RECEIVED_SIGNED: VIRTUAL.SIGNED,
    SEND_TO_FILL: VIRTUAL.PENDING,
    SENT_TO_SIGN: VIRTUAL.TO_SIGN,
    SENT_TO_FILL: VIRTUAL.PENDING,
    SENT_TO_DOWNLOAD: VIRTUAL.TO_SIGN
};

function hashCode(string) {
    let hash = 0,
        i,
        chr,
        len;
    if (string.length === 0) return hash;
    for (i = 0, len = string.length; i < len; i++) {
        chr = string.charCodeAt(i);
        hash = (hash << 5) - hash + chr;
        hash = hash | 0; // Convert to 32bit integer
    }
    return hash;
}

const userSchema = new Schema(keys, { timestamps: true, minimize: false });

userSchema.virtual('channel').get(function() {
    let number = hashCode(this._id.toString());
    if (number < 0) number = number * -1;
    number = number.toString();
    return `private-${number}`;
});

userSchema.virtual('fullName').get(function() {
    return `${this.givenName} ${this.familyName}`.trim();
});


userSchema.virtual('role', {
    ref: 'Role',
    localField: 'userRoles',
    foreignField: 'name',
    justOne: false
});

userSchema.methods.addCredit = function addCredit(amount, usage) {
    return Credit.create({
        user: this._id, type: '+', amount, usage
    });
};

userSchema.methods.monthBalanceRest = function monthBalanceRest(action) {
    return Credit.byTenant(this.organization).find({
        user: this._id,
        type: '-',
        createdAt: {
            $gte: moment().subtract(1, 'M').toDate(),
            $lte: moment().toDate()
        },
        usage: {
            $ne: 'endOfMonth'
        }
    }).then((credits) => {
        let creditsToAdd = 0;
        let creditsToSubstract = 0;
        let boughtCredits = 0;
        const creditsSpent = credits.length;
        this
            .getBalance()
            .then((balance) => {
                if (action === 'renew') {
                    const creditsAvailable = balance;
                    const creditsThisMonth = creditsSpent + creditsAvailable;

                    if (this.plan.name === 'V2_BASIC') {
                        if (creditsSpent > 10) boughtCredits = creditsAvailable;
                        else boughtCredits = creditsThisMonth - 10;
                        creditsToAdd = 10 - creditsAvailable;
                    } else if (this.plan.name === 'V2_CORPORATE') {
                        if (creditsSpent > 25) boughtCredits = creditsAvailable;
                        else boughtCredits = creditsThisMonth - 25;
                        creditsToAdd = 25 - creditsAvailable;
                    } else if (this.plan.name === 'V2_INNOVADOR') {
                        if (creditsSpent > 5) boughtCredits = creditsAvailable;
                        else boughtCredits = creditsThisMonth - 5;
                        creditsToAdd = 5 - creditsAvailable;
                    }
                    creditsToAdd = creditsToAdd + boughtCredits;
                    if (creditsToAdd > 0) {
                        return Credit.create({
                            user: this._id, type: '+', usage: 'subscriptionUpdate', amount: creditsToAdd
                        });
                    }
                } else {
                    if (action === 'V2_BASIC') {
                        if (creditsSpent < 10) creditsToSubstract = 10 - creditsSpent;
                    } else if (action === 'V2_CORPORATE') {
                        if (creditsSpent < 25) creditsToSubstract = 25 - creditsSpent;
                    }
                    if (creditsToSubstract > 0) {
                        return Credit.create({
                            user: this._id, type: '-', usage: 'subscriptionDelete', amount: creditsToSubstract
                        });
                    }
                }
                return Promise.resolve();
            });
    });
};

userSchema.methods.useCredit = function useCredit(options) {
    return this.getBalance().then((balance) => {
        if (balance > 0) {
            const json = options;
            json.user = this._id;
            json.type = '-';
            return Credit.create(json);
        }
        throw new Error('InsufficientCredit');
    });
};

userSchema.methods.getBalanceOld = function getBalanceOld() {
    const chargeDay = this.plan.chargeDay;
    const currentDay = moment().date();
    let startOfPeriod;
    let endOfPeriod;

    if (chargeDay > currentDay) {
        startOfPeriod = moment().set('date', chargeDay).subtract(1, 'months').startOf('day');
        endOfPeriod = moment().set('date', chargeDay).endOf('day');
    }

    if (chargeDay <= currentDay) {
        startOfPeriod = moment().set('date', chargeDay).startOf('day');
        endOfPeriod = moment().set('date', chargeDay).add(1, 'months').subtract(1, 'd')
            .endOf('day');
    }

    const matchCriteria = this.plan.type === 'contract' ?
        { user: this._id } :
        { user: this._id, createdAt: { $gte: startOfPeriod.toDate(), $lt: endOfPeriod.toDate() } };

    return Credit.byTenant(this.organization).aggregate([
        {
            $match: matchCriteria
        },
        {
            $group: {
                _id: '$type',
                amount: { $sum: '$amount' }
            }
        }
    ])
        .then((result) => {
            let adds = _.findWhere(result, { _id: '+' });
            let subs = _.findWhere(result, { _id: '-' });

            adds = adds ? adds.amount : 0;
            subs = subs ? subs.amount : 0;

            // const balance = adds - subs;
            const balance = 1000;
            return balance;
        });
};
userSchema.methods.getBalance = function getBalance(organization) {
    counter.getCount(organization).then((res) => res);
};

const fetchUserTasks = (user, organization) => Task.find({
    user,
    status: 'active'
})
    .sort({ createdAt: -1 })
    .lean()
    .exec();


const fetchApproved = function(user, organization) {
    return Contract.byTenant(organization).countDocuments({
        archived: false,
        $or: [
            {
                user,
                vobo: {
                    $elemMatch: {
                        status: 'approved',
                        user
                    }
                }
            },
            {
                user,
                status:'APPROVED'
            }
        ]
    });
};
const fetchSigned = function(user, organization) {
    return Contract.byTenant(organization).countDocuments({ deleted: { $ne: true },
        favorite: { $ne: true },
        status: { $in: [ 'RECEIVED_SIGNED' ] },
        parentReference: { $exists: false },
        $or: [ { user }, { watchers: user } ] }
    );
};

const fetchRevision = (user, organization) => {
    return Contract.byTenant(organization)
        .countDocuments({
            user,
            'archived': false,
            'revision.status': 'sent'
        });
};

const fetchNegotiation = (user, organization) => {
    return Contract.byTenant(organization)
        .countDocuments({ deleted: { $ne: true },
            favorite: { $ne: true },
            status: { $in: [ 'NEGOTIATION', 'PENDING_NEGOTIATION' ] },
            parentReference: { $exists: false },
            $or: [ { user }, { watchers: user } ] }
        );
};

const fetchApproval = (user, organization) => {

    return Contract.byTenant(organization)
        .countDocuments({ deleted: { $ne: true },
            favorite: { $ne: true },
            status: { $in: [ 'RECEIVED_FILLED', 'SENT_TO_SIGN', 'SENT_TO_DOWNLOAD' ] },
            parentReference: { $exists: false },
            $or: [ { user }, { watchers: user } ]
        });
};

const fetchFinished = (user, organization) => {
    return Contract.byTenant(organization)
        .countDocuments({ deleted: { $ne: true },
            favorite: { $ne: true },
            status: { $in: [ 'FINALIZED' ] },
            parentReference: { $exists: false },
            user
        });
};

const fetchWatched = (user, organization) => {
    return Contract.byTenant(organization).countDocuments(
        {
            watchers: { $elemMatch: { $in: [ new mongoose.Types.ObjectId(user) ] } },
            user: { $ne: user },
            archived: false,
            parentReference: { $exists: false }
        }
    );
};

const fetchDashboardInformation = (userId, organization) => Promise.all([
    fetchApproved(userId, organization),
    fetchRevision(userId, organization),
    fetchApproval(userId, organization),
    fetchFinished(userId, organization),
    fetchSigned(userId, organization),
    fetchNegotiation(userId, organization),
    fetchWatched(userId, organization)
])
    .then((results) => {
        return {
            approved: results[0],
            revision: results[1],
            validation: results[2],
            finished: results[3],
            signed: results[4],
            negotiation: results[5],
            watched: results[6],
            expiredSoon: []
        }
        ;
    });


const fetchInstantNotifications = (userId, tenantId, lastRead) => co(function *(){

  const notifications = yield InstantNotification.byTenant(tenantId)
     .find({ user: userId, type: { $ne: 'participants-signed' }, createdAt: { $gt: lastRead } })
     .populate('user', 'photo')
     .sort('-createdAt')
     // .limit(15)
     .exec();

     return notifications;
});

userSchema.methods.fetchMembers = function fetchMembers() {
    return this.model('User').byTenant(this.organization)
        .find({ $or:[
            { accountRoot: this._id },
            { organization: this.organization }
        ] })
        .select('givenName familyName photo email permission verified plan accountAdmin');
};

userSchema.methods.fetchGroups = function fetchMembers() {
    return this.model('User').byTenant(this.organization).findOne({ _id: this._id })
    .populate('groups')
    .then(function(u){
      return u.groups;
    });
};

userSchema.methods.fetchReferers = function fetchReferers() {
    return this.model('Referer').byTenant(this.organization).find({ refBy: this._id });
};

userSchema.methods.getPlan = function getPlan(userId) {
    if (!userId) return Promise.reject(new Error('ToGetPlanUserIsNecessary'));
    return this.model('User').byTenant(this.organization)
        .findById({ _id: userId })
        .then((result) => result && result.plan);
};

userSchema.methods.getTenantPlan = function getTenantPlan() {
    const self = this;
    return co(function* () {
        if (!self.organization) throw new Error('ToGetPlanUserIsNecessary');
        const result = yield self.model('Tenant').findOne({ organization: self.organization }).populate('planDetails').lean().exec();
        const fad = yield self.model('Contract').count({ fadSignature: { $exists: true }, tenantId: self.organization });
        const contractsCount = yield self.model('Contract').count({ status: { $nin: [''] }, tenantId: self.organization, deleted: { $ne: true }, archived: { $ne: true } });
        const fiel = yield self.model('Contract').count({ status:{ $in: ['RECEIVED_SIGNED', 'FINALIZED'] }, signatureType: 'certificate', tenantId: self.organization });
        const tenant = { plan: result.plan, name: result.name, contractsIncluded: result.contractsIncluded, startDate: result.startDate, balance: contractsCount, remaining: result.contractsIncluded - contractsCount };
        tenant.fadSignatures = result.fadSignatures - fad;
        tenant.fadSignaturesUsed = fad;
        tenant.fadSignaturesContracted = result.fadSignatures;
        tenant.fielSignatures = result.fielSignatures - fiel;
        tenant.fielSignaturesUsed = fiel;
        tenant.fielSignaturesContracted = result.fielSignatures;
        tenant.privileges = result.planDetails.privileges;
        if(result.kissFlow) tenant.kissFlow = true;
        return tenant;
    });
};
userSchema.methods.getRole = function getRole() {
    return this.model('User').byTenant(this.organization).findById(this._id)
    .populate('role')
    .then((user) => {
        let role = [];
        if(user && user.role) {
            const privs = _.pluck(user.role, 'privileges');
            privs.forEach((priv) => role = _.union(role, priv));
        }
      return role;
    });
};
userSchema.methods.can = function(priv) {
    const roles = userSchema.methods.getRole();
    if(roles && roles.includes(priv)) return true;
    return false;
};

userSchema.statics.can = function(id, tenantId, priv) {
    const User = mongoose.model('User');
    return User.byTenant(tenantId).findById(id)
            .populate('role')
            .then((user) => {
                let role = [];
                if(user && user.role) {
                    const privs = _.pluck(user.role, 'privileges');
                    privs.forEach((priv) => role = _.union(role, priv));
                }
                if(role && role.includes(priv)) return true;
                return false;
            });
};

userSchema.methods.hasSignedLicense = function hasSignedLicense(email) {
    return ContractLink
        .byTenant(this.organization)
        .find({ email, isLicense: true })
        .exec()
        .then((results) => {
            if (!results.length) return { signed: true };
            const cont = results[0];

            return Contract
                .findById(cont.contractid)
                .then((contract) => {
                    if (!contract) return { signed: true };
                    if (contract.status === 'RECEIVED_SIGNED') return { signed: true, contract: contract._id };
                    return { signed: false, contract: contract._id };
                });
        });
};

userSchema.methods.getDashboardInformation = function() {
    return Promise.all([
        fetchInstantNotifications( (this.owner !== undefined) ? this.owner : this._id, this.organization, this.lastRead),
        fetchDashboardInformation( (this.owner !== undefined) ? this.owner : this._id, this.organization),
        fetchUserTasks(this.owner || this._id, this.organization)
    ]).then((results) => {
        const notifications = results[0];
        const counters = results[1];
        const tasks = results[2];

        return { notifications, counters, tasks };
    });
};

userSchema.methods.getPublicProfile = function getPublicProfile() {

    return Promise.all([
        this.getBalance(this.owner),
        fetchInstantNotifications( (this.owner !== undefined) ? this.owner : this._id, this.organization, this.lastRead),
        fetchDashboardInformation( (this.owner !== undefined) ? this.owner : this._id, this.organization),
        this.fetchMembers(this.owner || this._id, this.organization),
        this.getPlan(this.owner || this.accountRoot || this._id),
        fetchUserTasks(this.owner || this._id, this.organization),
        this.hasSignedLicense(this.email),
        this.fetchGroups(),
        this.getTenantPlan(),
        this.getRole()
    ]).then((results) => {
      // return {};
        const balance = results[0];
        const notifications = results[1];
        const dashboard = results[2];
        const members = results[3];
        const plan = results[4] || this.plan;
        const tasks = results[5];
        const license = results[6];
        const groups = results[7];
        const tenant = results[8];
        const role = results[9];
        const profile = {
            _id: this._id,
            admin: this.admin,
            givenName: this.givenName,
            isAuthor: this.isAuthor,
            familyName: this.familyName,
            email: this.email,
            photo: this.photo,
            fullName: this.fullName,
            channel: this.channel,
            accountAdmin: this.accountAdmin,
            plan,
            groups,
            roles: this.roles,
            office: this.office,
            lastRead: this.lastRead,
            demo: this.demo,
            accountRoot: this.accountRoot,
            userIsClient: this.userIsClient,
            language: this.language,
            invoice: this.invoice,
            lawyerOnly: this.lawyerOnly,
            hasCancelled: this.hasCancelled,
            hasOwner: Boolean(this.owner),
            permission: this.permission,
            stripeCustomerId: this.stripeCustomerId,
            phone: this.phone,
            authorName: this.authorName,
            members,
            balance,
            dashboard,
            twoFactor: this.twoFactor,
            notifications,
            tasks,
            signature: this.signature,
            license,
            organization: this.organization,
            users: {
                total: tenant.contractsIncluded,
                available: tenant.contractsIncluded - (members.length + 1),
                assigned: members.length + 1
            },
            tenant,
            role
        };
        plan.users = tenant.contractsIncluded;

        return profile;
    });
};

// userSchema.methods.sendVerificationMailWithPassword = function sendVerificationMailWithPassword(prefix, pwd) {
//     const addPrefx = prefix || '/';
//
//     return emails.sendVerificationMailWithPassword({
//         prefix,
//         to: this.email,
//         url: `${process.env.url}/verify${addPrefx}${this.verificationHash}`,
//         name: this.fullName,
//         password: pwd
//     });
// };

// userSchema.methods.sendVerificationMail = function sendVerificationMail(prefix) {
//     const addPrefx = prefix || '/';
//     return emails.verificationMail({
//         prefix,
//         to: this.email,
//         url: `${process.env.url}/verify${addPrefx}${this.verificationHash}`,
//         name: this.fullName
//     }
//     );
// };

userSchema.methods.acceptInvitation = function acceptInvitation(refererHash) {
    return Referer.update({ verificationHash: refererHash }, { status: 'accepted' }).exec();
};

userSchema.methods.hasContracts = function hasContracts() {
    return Contract.byTenant(this.user.organization).findOne({ user: this._id }).then((contract) => Boolean(contract));
};

// userSchema.methods.intercom = function(flow) {
//     if (flow === 'subscribe') return intercom.createUser(this);
//     if (flow === 'purchase-plan') return intercom.setUserPlan(this);
// };

userSchema.methods.agile = function(flow) {
    if (flow === 'subscribe') return agile.createUser(this);
};

userSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('User', userSchema);
