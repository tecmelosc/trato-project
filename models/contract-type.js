

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const participantSchema = new Schema({
    label: String
}, { minimize: false, timestamps: true });

const declarationSchema = new Schema({
    title: String,
    content: String,
    participantIndex: {
        type: Number,
        default: 0
    },
    type: String
}, { minimize: false, timestamps: true });

const clauseSchema = new Schema({
    title: String,
    content: String,
    participantIndex: {
        type: Number,
        default: 0
    },
    type: String
}, { minimize: false, timestamps: true });

const contractTypeSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    jurisdiction: String,
    participants: [ participantSchema ],
    matter: {
        type: Schema.Types.ObjectId,
        //required: true,
        ref: 'Matter'
    },
    active: {
        type: Boolean,
        default: true
    },
    default:{
        type: Boolean
    },
    declarations: [ declarationSchema ],
    clauses: [ clauseSchema ]
}, { minimize: false, timestamps: true });

contractTypeSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('ContractType', contractTypeSchema);
