

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const signatureSchema = new Schema({
    email: String,
    video: String,
    givenName: String,
    familyName: String,
    salt: {
        type: String,
        trim: true,
        required: true
    },
    companyDomain: String
}, { timestamps: true });

signatureSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Signature', signatureSchema);
