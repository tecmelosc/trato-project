

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;
const Types = Schema.Types;

const tokenSchema = new Schema({
    contract: {
        type: Types.ObjectId,
        ref: 'Contract'
    },
    jwt: {
        type: String,
        required: true,
        trim: true
    },
    token: {
        type: String,
        required: true,
        trim: true
    },
    participant: {}
}, { timestamps: true });

tokenSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Token', tokenSchema);
