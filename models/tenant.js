

const mongoose = require('mongoose');
const tenantConfigProperties = require('./subschemas/tenant-config-properties');
const kissFlowSchema = require('./subschemas/kissflow');
const Schema = mongoose.Schema;
const tenantSchema = new Schema(
    {
        name: {
            type: String,
            index: true,
            unique: true
        },
        plan: String,
        planId: String,
        organization: {
            type: String,
            index: true,
            unique: true
        },
        contractsIncluded: Number,
        startDate: Date,
        domain: String,
        fadSignatures: Number,
        config: {
            taketemplatesmax: Number,
            taketemplatesperiodicity: String,
            view_shared_templates: Boolean,
            blockchainenabled: Boolean,
            maxmigrated: Number,
            restrictparticipantview: Boolean
        },
        usedspace: Number,
        // kissFlow: kissFlowSchema,
        // referer: {
        //     type: Schema.Types.ObjectId,
        //     ref: 'User'
        // }
    },
    { timestamps: true }
);
tenantSchema.virtual('planDetails', {
    ref: 'Plan',
    localField: 'plan',
    foreignField: 'name',
    justOne: true
});
tenantSchema.set('toObject', { virtuals: true });
tenantSchema.set('toJSON', { virtuals: true });
module.exports = mongoose.model('Tenant', tenantSchema);
