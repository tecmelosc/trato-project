

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const contractLinkSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    archived: {
        type: Boolean,
        default: false
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User',
        index: true
    },
    isLicense: {
        type: Boolean,
        default: false
    },
    email: {
        type: String,
        trim: true
        /*validate(value) {
            return value.indexOf('@') > -1 || !value;
        }*/
    },
    contractid: {
        type: Schema.Types.ObjectId,
        required: true,
        index: true
    },
    status: {
        type: String,
        default: ''
    },
    type: {
        type: String,
        default: ''
    },
    childOf: {
        type: String,
        default: undefined
    }
}, { timestamps: true });

contractLinkSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('ContractLink', contractLinkSchema);
