const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const taskSchema = new Schema({
    contract: Schema.Types.ObjectId,
    type: String,
    status: String,
    user: Schema.Types.ObjectId,
    properties: Object
}, { timestamps: true });

taskSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Task', taskSchema);
