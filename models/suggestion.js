

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const suggestionSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    content: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }
}, { timestamps: true });

suggestionSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Suggestion', suggestionSchema);
