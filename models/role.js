const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RoleSchema = new Schema({
    name: {
        type: String,
        index: true,
        unique: true,
        require: true
    },
    privileges: {
        type: Array,
        require: true
    }
}, { timestamps: true });

module.exports = mongoose.model('Role', RoleSchema);
