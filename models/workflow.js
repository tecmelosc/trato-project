const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');
const Schema = mongoose.Schema;
const workflowsteps = require('./subschemas/workflowsteps');

const workflowSchema = new Schema({
    id: String,
    name: String,
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    steps: [ workflowsteps ]
}, { timestamps: true });

workflowSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Workflow', workflowSchema);
