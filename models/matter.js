

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const matterSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    jurisdiction: String,
    active: {
        type: Boolean,
        default: true
    }
}, { minimize: false, timestamps: true });

matterSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Matter', matterSchema);
