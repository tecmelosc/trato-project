

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const elementSchema = require('./subschemas/element');
const templateParticipant = require('./subschemas/template-participant');
const spectatorSchema = require('./subschemas/spectator');
const templateQuestion = require('./subschemas/template-question');
const attachmentSchema = require('./subschemas/attachment');
const subordinationSchema = require('./subschemas/subordination');
const workdocsSchema = require('./subschemas/workdocs');
const revisionSchema = require('./subschemas/revision');
const voboSchema = require('./subschemas/voboSchema');
const tagSchema = require('./subschemas/tags');
const wordOnlineSchema = require('./subschemas/wordonline');
const notificationSchema = require('./subschemas/notification');
const elementCommentSchema = require('./subschemas/element-comment');
const changeRequestSchema = require('./subschemas/change-request');
const templateSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    workdocs: workdocsSchema,
    importedPdf: {
        type: String
    },
    importedWord: {
        type: String
    },
    enableNotifications: {
        type: Boolean,
        default: false
    },
    notifications: [notificationSchema],
    subordination: subordinationSchema,
    archived: {
        type: Boolean,
        default: false
    },
    workflow: {
        type: Schema.Types.ObjectId,
        ref: 'Workflow'
    },
    workflowStep: {
        type: Number,
        default: 0,
    },
    conversations: [elementCommentSchema],
    workflowStopped: {
        type: Boolean
    },
    description: {
        type: String,
        trim: true
    },
    jurisdiction: {
        type: String,
        default: 'mx'
    },
    type: {
        type: Schema.Types.ObjectId,
        ref: 'ContractType'
    },
    templateType: {
        type: String
    },
    database: {
        type: Schema.Types.ObjectId,
        required: false,
        ref: 'Database',
        index: true
    },

    authorName: String,
    vobo: [voboSchema],
    prefilled: Boolean,
    private: {
        type: Boolean,
        default: true
    },
    tags: [tagSchema],
    contract: {
        type: Schema.Types.ObjectId,
        ref: 'Contract'
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    elements: [elementSchema],
    options: {
        type: Object,
        default: {
            clauseStyle: 'simple'
        }
    },
    pdf: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: 'private'
    },
    lawyer: {
        type: Boolean,
        default: false
    },
    price: {
        type: Number
    },
    orientationPrice: {
        type: Number
    },
    orientationDescription: {
        type: String,
        default: ''
    },
    hasOrientation: {
        type: Boolean,
        default: false
    },
    boughtOrientation: {
        type: Boolean,
        default: false
    },
    participants: [templateParticipant],
    spectators: [spectatorSchema],
    template: {
        type: Schema.Types.ObjectId,
        ref: 'Template',
        index: true
    },
    clientShared: {
        type: Boolean
    },
    deleted: {
        type: Boolean,
        default: false
    },
    favorite: {
        type: Boolean,
        default: false
    },
    boughtCount: {
        default: 0,
        type: Number
    },
    publishedAt: {
        type: Date,
        default: null
    },
    rejectedAt: {
        type: Date,
        default: null
    },
    buyers: [{
        type: Schema.Types.ObjectId,
        ref: 'User',
        index: true
    }],
    locked: {
        type: Boolean,
        defualt: false
    },
    verified: {
        type: Boolean,
        defualt: false
    },
    childOf: {
        type: [{
            type: String,
            ref: 'Folder'
        }],
        default: []
    },
    watchers: [{
        type: Schema.Types.ObjectId,
        ref: 'User',
        index: true
    }],
    revision: [revisionSchema],
    questions: [templateQuestion],
    wordonline: wordOnlineSchema,
    originalFileType: String,
    duplicatedTemplates: [{
        type: Schema.Types.String,
        ref: 'Template'
    }
    ],
    duplicatedFrom: {
        type: Schema.Types.String,
        ref: 'Template'
    },
    originalOwner: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    changeRequests: [changeRequestSchema]
}, { timestamps: true });

templateSchema.methods.isOwner = function (userId) {
    return this.user && this.user.toString() === userId;
};

templateSchema.methods.isBought = function () {
    return this.status === 'bought' && this.template;
};

templateSchema.methods.resetBought = function () {
    return this.model('Template').findById(this.template).populate('type').then((originalTemplate) => {
        if (!originalTemplate) throw new Error('OriginalTemplateNotFound');
        this.elements = originalTemplate.elements;
        this.participants = originalTemplate.participants;
        this.name = originalTemplate.name;
        this.description = originalTemplate.description;
        this.type = originalTemplate.type;
        return this.save();
    });
};

templateSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Template', templateSchema);
