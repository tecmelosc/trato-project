

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const sharedTemplateSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    template: {
        type: Schema.Types.ObjectId,
        ref: 'Template',
        index: true
    },
    email: String,
    message: String,
    sharedDate: Date

}, { timestamps: true });

sharedTemplateSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('SharedTemplate', sharedTemplateSchema);
