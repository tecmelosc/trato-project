const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const notificationSchema = new Schema({
    notificationDate: Date,
    message: String,
    contract: String,
    user: {
        name: String,
        email: String
    },
    daysBefore: Array,
    type: String,
    active: Boolean
}, { timestamps: true });

notificationSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Notification', notificationSchema);
