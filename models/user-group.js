

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const userModel = mongoose.model('User');

const userGroupSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    admin: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    // parent: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'UserGroup'
    // },
    canSend: {
        type: Schema.Types.Boolean,
        default: false
    },
    organization: {
        type: String
    }
},
{ timestamps: true }
);


userGroupSchema.methods.members = () => userModel
    .byTenant(this.organization)
    .find({ groups: { $in: [ this._id ] } })
    .select('email familyName givenName photo groups')
    .then((users) => users);

userGroupSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('UserGroup', userGroupSchema);
