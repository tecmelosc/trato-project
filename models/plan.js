const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PlanSchema = new Schema({
    name: {
        type: String,
        index: true,
        unique: true
    },
    privileges: {
        type: Array
    }
}, { timestamps: true });

module.exports = mongoose.model('Plan', PlanSchema);
