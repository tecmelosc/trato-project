

const mongoose = require('mongoose');
// const GenerateContract = require('../controllers/contracts/pdf');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;
const hummus = require('hummus');
const r = require('request');
const i2b = require('imageurl-base64');

const _ = require('underscore');
const contractPropertiesSchema = require('./subschemas/contract-properties');
const elementSchema = require('./subschemas/element');
const historySchema = require('./subschemas/history');
const participantSchema = require('./subschemas/participant');
const commentSchema = require('./subschemas/comment');
const elementCommentSchema = require('./subschemas/element-comment');
const attachmentSchema = require('./subschemas/attachment');
const tagSchema = require('./subschemas/tags');
const changelogSchema = require('./subschemas/changelog');
const blockchainSchema = require('./subschemas/blockchain');
const guestSchema = require('./subschemas/guest');
const revisionSchema = require('./subschemas/revision');
const referenceSchema = require('./subschemas/reference');
const voboSchema = require('./subschemas/voboSchema');
const notificationSchema = require('./subschemas/notification');
const spectatorSchema = require('./subschemas/spectator');
const lawSchema = require('./subschemas/laws');
const subordinationSchema = require('./subschemas/subordination');
const automationSchema = require('./subschemas/automation');
const workdocsSchema = require('./subschemas/workdocs');
const wordOnlineSchema = require('./subschemas/wordonline');
const fadCertificateSchema = require('./subschemas/fadcertificate');
const tinyCommentsSchema = require('./subschemas/tinycomments');
const delegateSchema = require('./subschemas/delegation');
const paymentSchema = require('./subschemas/payment');
// const s3 = require('../services/s3');

const validSignatureTypes = [ 'autograph', 'certificate', 'none', 'simple', 'fad' ];
const Folder = require('./folder');
const fs = require('fs');
const v1 = require('uuid').v1;

const PDFDocument = require('pdfkit');
const contractSchema = new Schema({
  payments: [ paymentSchema ],
  tinyComments: [ tinyCommentsSchema ],
    hideAttachments: {
        type: Boolean,
        default: false
    },
    hideMobileHeader: {
        type: Boolean,
        default: false
    },
    tenantId:{
        type: String,
        required:true
    },
    enableNotifications: {
      type: Boolean,
      default: false
    },
    workdocs: workdocsSchema,
    wordonline: wordOnlineSchema,
    guests: [ guestSchema ],
    notifications: [ notificationSchema ],
    language: {
        type: String,
        default: 'ES'
    },
    editorType: {
      type: String,
      default: 'basic'
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    clicked: {
      type: Boolean,
      default: true
    },
    amountSingle: {
      type: Boolean,
      default: true
    },
    stage: String,
    reference: [ referenceSchema ],
    spectators: [ spectatorSchema ],
    countByTemplate: Boolean,
    counterByTemplate: {
        type: Number,
        default: 0
    },
    database: {
      type: Schema.Types.ObjectId,
      required: false,
      ref: 'Database',
      index: true
    },
    countByOrganization: Boolean,
    counterByOrganization: {
        type: Number,
        default: 0
    },
    showPageNumber: {
        type: Boolean,
        default: true
    },
    negotiate: {
        type: Boolean,
        default: false
    },
    laws: [ lawSchema ],
    stampNom: {
        type: Boolean,
        default: false
    },
    allowAttachments: {
        type: Boolean,
        default: true
    },
    expiryUndefined: {
        type: Boolean,
        default: false
    },
    alreadyCharged: {
        type: Boolean,
        default: false
    },
    validationRequired: {
        type: Boolean,
        default: false
    },
    notifyall: {
        type: Boolean,
        default: false
    },
    stampApproval: {
        type: Boolean,
        default: true
    },
    license: {
        type: Boolean,
        default: false
    },
    archived: {
        type: Boolean,
        default: false
    },
    description: {
        type: String,
        trim: true
    },
    template: {
        type: Schema.Types.ObjectId,
        ref: 'Template',
        index: true
    },
    jurisdiction: {
        type: String,
        default: 'mx',
        enum: [ 'mx', 'us' ]
    },
    type: {
        type: Schema.Types.ObjectId,
        ref: 'ContractType',
        index: true
    },
    uploadMethod:{
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        required: false,
        ref: 'User',
        index: true
    },
    creator: {
        type: Schema.Types.ObjectId,
        required: false,
        ref: 'User',
        index: true
    },
    userGroup: {
        type: Schema.Types.ObjectId,
        required: false,
        ref: 'UserGroup',
        index: true
    },
    watchers: [ {
        type: Schema.Types.ObjectId,
        required: false,
        ref: 'User',
        index: true
    } ],
    shareProperties: {},
    tags: [ tagSchema ],
    automation: [ automationSchema ],
    elements: [ elementSchema ],
    attachments: [ attachmentSchema ],
    extraAttachments: [ attachmentSchema ],
    options: {
        type: Object,
        default: {
            clauseStyle: 'simple',
            audio: false,
            video: false,
            attachments: false,
            attachmentsRequired: false,
            photo: false
        }
    },
    blockchain: blockchainSchema,
    validated: {
      type: Boolean,
      default: false
    },
    blockchainTimestamp: {
        type: Boolean,
        default: false
    },
    dateSent: {
        type: Date
    },
    signedAt: {
        type: Date
    },
    transactionId: String,
    webhook: String,

    notificationType: {
        type: String,
        default: 'email'
    },

    signatureType: {
        type: String,
        default: 'none',
        validate(value) {
            return validSignatureTypes.indexOf(value) > -1;
        }
    },
    userIsParticipant: {
        type: Schema.Types.ObjectId
    },
    status: {
        type: String,
        default: ''
    },
    googleForms: {},
    conversations: [ elementCommentSchema ],

    comments: [ commentSchema ],
    reads: Schema.Types.Mixed,
    history: [ historySchema ],
    editable: {
        type: Boolean,
        default: true
    },
    variablesCompleted: {
        type: Boolean,
        default: false
    },
    favorite: Boolean,
    participants: [ participantSchema ],
    password: String,
    expiryStartDate: Date,
    expiryEndDate: Date,
    wsid: Number,
    originalPdfUrl: String,
    downloadPdfUrl: String,
    signedPdfUrl: String,
    nom151: String,
    signedPdf: String,
    revision: [ revisionSchema ],
    vobo: [ voboSchema ],
    subordination: subordinationSchema,
    prefilled: {
        type: Boolean,
        default: false
    },
    lastThesisSearch: String,
    duplicateOf: {
        type: Schema.Types.ObjectId,
        ref: 'Contract',
        index: true
    },
    isLink: {
        type: Boolean,
        default: false
    },
    deleted: {
        type: Boolean,
        default: false
    },
    locked: {
        type: Boolean,
        default: false
    },
    alreadySent: {
        type: Boolean
    },
    importedPdf: {
        type: String
    },
    importedWord: {
        type: String
    },
    height: Number,
    width: Number,
    childOf: {
        type: [ {
            type: String,
            ref: 'Folder'
        } ],
        default: []
    },
    hash: {
        type: String
    },
    ownerSelfSign: {
        type: Boolean
    },
    parentReference:{
        type: Schema.Types.ObjectId,
        ref: 'Contract'
    },
    changelog: [ changelogSchema ],
    fadSignature: {
        type: Schema.Types.ObjectId,
        ref: 'FadSignature'
    },
    workflow: {
        type: Schema.Types.ObjectId,
        ref: 'Workflow'
    },
    workflowStep : {
        type: Number,
        default: 0,
    },
    workflowStopped : {
        type: Boolean
    },
    properties: contractPropertiesSchema,
    documentId: {
        type: Number,
        default: '',
        index: true
    },
    delegation: [ delegateSchema ],
    openedAfterDelegation: {
        type: Boolean
    },
    fadCertificate: [ fadCertificateSchema ],
    tinymceImages : { type: Array },
    signInOrder: {
        type: Boolean
    }
},
{
    timestamps: true,
    usePushEach: true
}
);

// contractSchema.methods.getPdfUrl = function getPdfUrl() {
//     return new Promise((resolve, reject) => {
//         if (this.downloadPdfUrl) return resolve(this.downloadPdfUrl);
//         if (this.importedPdf) return resolve(this.importedPdf);
//         if (this.originalPdfUrl) return resolve(this.originalPdfUrl);
//
//         return GenerateContract({ contract: this, expires: -1 })
//             .then((url) => {
//                 if (!url) throw new Error('PDFServiceError');
//
//                 // this.downloadPdfUrl = url;
//                 // this.save().then(function(){
//                 resolve(url);
//                 // });
//             })
//             .catch((err) => reject(err));
//     });
// };

const getS3Buffer = (url) =>
    new Promise((resolve, reject) => {
        r(
            {
                uri: url,
                encoding: null
            },
            (err, result) => {
                if (err) return reject(err);
                return resolve(result.body);
            }
        );
    });

function executePDFStamp(pdf, signatures, image) {
    const signatureWidth = 250 / 72 * 65.4;
    const pdfFile = `/tmp/in_${v1()}.pdf`;
    fs.writeFileSync(pdfFile, pdf, 'binary');

    const stampItem = (signatureBuffer, sign) => new Promise((resolve) => {
        const left = sign.left / 72 * 65.4;
        const top = sign.top / 72 * 65.4;
        const width = sign.width / 72 * 65.4;
        const height = sign.height / 72 * 65.4;

        const out = `/tmp/signature_${sign._id}.pdf`;
        const doc = new PDFDocument({ size: [ width, height ] });

        const stream = doc.pipe(fs.createWriteStream(out));
        doc.image(signatureBuffer, left, top, { width: signatureWidth });
        doc.end();

        stream.on('finish', () => {
            const writer = hummus.createWriterToModify(pdfFile);
            const pageToStamp = Number(sign.page) - 1;
            const modifier = new hummus.PDFPageModifier(writer, pageToStamp, true);
            const ctx = modifier.startContext().getContext();
            ctx.drawImage(0, 0, out);
            modifier.endContext().writePage();
            writer.end();
            resolve();
        });
    });

    return new Promise((resolve, reject) => {
        const isURL = image.indexOf('http') !== -1;

        if (isURL) {
            i2b(image, (err, data) => {
                if (err) reject(err);
                else resolve(new Buffer(data.base64, 'base64'));
            });
        } else resolve(new Buffer(image.split('data:image/png;base64,')[1], 'base64'));
    })
        .then((imageSignature) => Promise.mapSeries(signatures, (sign) => stampItem(imageSignature, sign)).then(() => pdfFile));
}
//
// contractSchema.methods.stamp = function stamp(participant) {
//     const self = this;
//     const image = participant.signature;
//     const signatures = participant.signatures;
//
//     return getS3Buffer(this.importedPdf)
//         .then((pdf) => executePDFStamp(pdf, signatures, image))
//         .then((file) => {
//             const result = fs.readFileSync(file);
//             return s3.uploadAndGetKey({
//                 Bucket: process.env.eclairBucket,
//                 Body: result,
//                 ContentEncoding: 'base64',
//                 ContentType: 'application/pdf',
//                 ACL: 'public-read',
//                 Extension: 'pdf'
//             });
//         })
//         .then((url) => {
//             console.log('Stamped', url);
//             self.importedPdf = url;
//             self.save();
//         });
// };

contractSchema.pre('save', function(next) {
    this.childOf = this.childOf || [];
    if (this.childOf.length)
        return next();
    Folder/*.byTenant(tenantId)*/.findOneAndUpdate({
        name:'root',
        user: this.user
    },
    {
        $set:{
            name:'root',
            user: this.user
        }
    },
    {
        new: true,
        upsert: true
    })
        .lean()
        .exec()
        .then((folder) => {
            this.childOf.push(folder._id);
            next();
        })
        .catch((err) => {
            console.log(err);
            next();
        });
});

contractSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Contract', contractSchema);
