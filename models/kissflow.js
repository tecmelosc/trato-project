

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const kissFlowSchema = new Schema({
    name: {
        type: String,
        index: true
    },
    kissflowId: {
        type: String,
        index: true
    },
    email: {
        type: String
    },
    participantName: {
        type: String
    },
    participantRepresentative: {
        type: String
    }
}, { minimize: false, timestamps: true });

kissFlowSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Kissflow', kissFlowSchema);
