const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

// const validTypes = ['payment', 'paydate', 'event'];
// const validFormats = ['cron', 'fixed'];

const pixelSchema = new Schema({
    type: String,
    email: String,
    ip: String
}, { timestamps: true });

pixelSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Pixel', pixelSchema);
