const mongoose = require('mongoose');
//const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;
const evicertiaSignature = new Schema({
    requisitionId: {
        type: Schema.Types.String,
        index: true
    },
    event: Schema.Types.Mixed
}, { timestamps: true });

module.exports = mongoose.model('EvicertiaSignature', evicertiaSignature);
