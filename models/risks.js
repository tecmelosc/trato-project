

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const risksSchema = new Schema({
    contract: {
      type: Schema.Types.ObjectId,
      ref: 'Contract'
    },
    type: String,
    status: String,
    user:{
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    closedAt: Date

}, { timestamps: true });

risksSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Risks', risksSchema);
