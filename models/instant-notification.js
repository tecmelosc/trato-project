

const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');

const Schema = mongoose.Schema;

const instantNotificationSchema = new Schema({
    type: String,
    participantName: String,
    contractName: String,
    contract: {
        type: Schema.Types.ObjectId,
        ref: 'Contract'
    },
    who: String,
    to: String,
    user_to: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    message: String,
    read: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

instantNotificationSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('InstantNotification', instantNotificationSchema);
