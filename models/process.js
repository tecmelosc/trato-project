const mongoose = require('mongoose');
const mongoTenant = require('mongo-tenant');
const Schema = mongoose.Schema;

const processSchema = new Schema({
    id: String,
    type: String,
    active: {
        type: Boolean,
        default: true
    },
    processed: [{
        type: Schema.Types.ObjectId,
        ref: 'Contract'
    }],
    error: [Schema.Types.Mixed],
    progress: Number,
    success: {
        type: Boolean
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, { timestamps: true });

processSchema.plugin(mongoTenant, { enabled: process.env.tenantsEnabled === 'true' || process.env.tenantsEnabled === true });

module.exports = mongoose.model('Process', processSchema);
