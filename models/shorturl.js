const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ShorturlSchema = new Schema(
    {
        originalUrl: { type: String },
        shortUrl: { type: String },
        errorUrl: { type: String },
        urlCode: {
            type: String,
            index: true,
            unique: true
        }
    },
    { timestamps: true }
);

module.exports = mongoose.model('Shorturl', ShorturlSchema);
