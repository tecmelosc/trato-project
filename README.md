# Trato Technical Examn

This project is a CRUD of users, it's developed in JavaScript using nodeJS. The data base is built in MongoDB a no SQL database admin.


Project url:
http://134.209.227.213

## Installation

Install all the dependencies of the project.

```bash
npm install
```
Run in a command line the node server

```bash
npm run start
```

For run in a  dev environment use for watch every change and recompile automatic

```bash
npm run dev
```
