const express = require('express');
const path = require('path');
const app = express();
const mongoose=require('mongoose')
const bodyParser=require('body-parser')
const routes= require('./routes/index');
const cors= require('cors')
const RateLimit = require('express-rate-limit');
const aws = require('aws-sdk');
const requestIp = require('request-ip');


app.set('port',3000)

const limiter = new RateLimit({
    windowMs: 60 * 60 * 1000,
    max: !isNaN(10000) && parseInt(process.env.LIMIT_MAX, 10) || 300000,
    delayMs: 0
});
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});
app.use(limiter);
app.use(bodyParser({ limit: '90mb' }));
app.use(bodyParser.json({ limit: '90mb', extended: true, parameterLimit: 500000 }));
app.use(bodyParser.json({ limit: '90mb', extended: true, parameterLimit: 500000, type: 'application/json' }));
app.use(bodyParser.urlencoded({ limit: '90mb', extended: true, parameterLimit: 500000 }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(requestIp.mw());
app.use(cors({ credentials: true, origin: true, optionsSuccessStatus: 200 }));

//Routes
app.use(routes);

app.enable('trust proxy');
//DB Connection
mongoose.connect("mongodb://test:test123@ds343895.mlab.com:43895/technicaltest", {
    db:"technicaltest",
    user:"testuser",
    pass:"test123"
}, (err,res) => {
    if(err) throw err;
    console.log("DB online");
});

app.listen(3000, () => {
    console.log("Server on port: ", app.get('port'));
});

module.exports = app;

